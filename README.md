# ansible-init

Quickly creates directory / file structure for working in playbooks.

From a playbook dir

```shell
git submodule add ssh://git@bitbucket.org/hpadrick/ansible-init.git init
```

Modify your make file to include the init.mk file:

```shell
echo 'include init/*.mk' >> makefile
```

## Create Environment

```shell
make env -e env=production
```

## Create Role

```shell
make role -e role=nvm-node
```

The resulting struture from the above commands will be:

```shell
.
├── environments
│   ├── 000_shared_vars.yml
│   └── production
│       ├── group_vars
│       │   └── all
│       │       ├── 000_shared_vars.yml -> ../../../000_shared_vars.yml
│       │       └── env_specific.yml
│       └── hosts
├── init
│   ├── init.mk
│   ├── init.yml
│   └── README.md
├── makefile
└── roles
    └── nvm-node
        ├── files
        ├── handlers
        │   └── main.yml
        ├── meta
        ├── tasks
        │   └── main.yml
        ├── templates
        └── vars

```
