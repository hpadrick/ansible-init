# vim: set et!:

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))
project_dir := $(shell pwd)

role:
	ansible-playbook -t "create-role" -e "role=${role} project_dir=${project_dir}" ${current_dir}/init.yml

env:
	ansible-playbook -t "create-env" -e "env=${env} project_dir=${project_dir}" ${current_dir}/init.yml

